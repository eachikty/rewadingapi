'use strict';

const Hapi = require('hapi');
const Boom = require('boom');
const Pack = require('./package');
const HapiSwagger = require('hapi-swagger');
const SocketIO = require('socket.io');
const request = require('request');
var ecobin01_usr = null;

// Create a server with a host and port
const server = Hapi.server({
    host: 'localhost', //172.20.10.8
    port: 8000,
    routes: {
        cors: true
    }
});
const io = SocketIO.listen(server.listener);
const swaggerOptions = {
    info: {
        title: 'eco bin API Documentation',
        version: Pack.version,
    },
};

const dbOpts = {
    url: 'mongodb://localhost:2222/ecobin', //mongodb://localhost:2222/ecobin
    settings: {
        poolSize: 10
    },
    decorate: true
};

server.route({
    method: 'POST',
    path: '/ecobin/login',
    config: {
        tags: ['api'],
        description: 'Add reward history',
        async handler(request) {

            const db = request.mongo.db;

            try {
                if (request.payload.bin_id == "ecobin001-bangsean") {
                    ecobin01_usr = request.payload.usr_id
                        // io.emit('realtimeview', "login naja eiei");
                        // broadcastData()
                    console.log(ecobin01_usr)
                }
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

// function broadcastData() {
//     request.get('http://localhost:8000/throwAwayHistorys', function(error, response, body) {
//         io.emit('realtimeview', body);
//     });
// }


server.route({
    method: 'POST',
    path: '/ecobin/logout',
    config: {
        tags: ['api'],
        description: 'Add reward history',
        async handler(request) {

            const db = request.mongo.db;

            try {
                ecobin01_usr = null
                console.log(ecobin01_usr)
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

// Add the route
server.route({ // GET content
    method: 'GET',
    path: '/users/{username}/{password}', //path'
    config: {
        auth: false,
        description: 'Get user',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('users').find().toArray();
            var usr_data;

            for (let i = 0; i < res.length; i++) {
                if (res[i].username == req.params.username && res[i].password == req.params.password)
                    usr_data = res[i];
            }

            return {
                data: usr_data,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});

server.route({ // GET content
    method: 'GET',
    path: '/historyAll', //path'
    config: {
        auth: false,
        description: 'Get promotion',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            var res = await db.collection('throwAwayHistorys').aggregate([{
                '$group': {
                    '_id': {
                        'year': { '$substr': ['$date_time', 0, 4] },
                        'month': { '$substr': ['$date_time', 5, 2] }
                    },
                    'glass': { $sum: '$glass' },
                    'metal': { $sum: '$metal' },
                    'plastic': { $sum: '$plastic' },
                }
            }]).toArray();

            return {
                data: res,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});

server.route({ // GET content
    method: 'GET',
    path: '/promotions', //path'
    config: {
        auth: false,
        description: 'Get promotion',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('promotions').find().toArray();
            return {
                data: res,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});

server.route({ // GET content
    method: 'GET',
    path: '/rewards', //path'
    config: {
        auth: false,
        description: 'Get reward',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('rewards').find().toArray();
            return {
                data: res,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});

server.route({ // GET content
    method: 'GET',
    path: '/rewards/rewardHistorys', //path'
    config: {
        auth: false,
        description: 'Get reward history',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('rewardHistorys').find().toArray();
            return {
                data: res,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});

server.route({ // GET content
    method: 'GET',
    path: '/throwAwayHistorys', //path'
    config: {
        auth: false,
        description: 'Get throw away history',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('throwAwayHistorys').find().toArray();
            return {
                data: res,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});


server.route({ // GET content
    method: 'GET',
    path: '/bin', //path'
    config: {
        auth: false,
        description: 'Get Bin data ',
        tags: ['api']
    },
    handler: async(req, reply) => {
        try {
            const db = req.mongo.db;
            const res = await db.collection('bin').find().toArray();
            return {
                data: res,
                message: 'OK',
                statusCode: 200,
            };

        } catch (error) {
            return (Boom.badGateway(error));
        }
    }
});



server.route({
    method: 'POST',
    path: '/rewards/rewardHistory/add',
    config: {
        tags: ['api'],
        description: 'Add reward history',
        async handler(request) {

            const db = request.mongo.db;

            try {
                await db.collection('rewardHistorys').insert(request.payload);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'POST',
    path: '/users/add',
    config: {
        tags: ['api'],
        description: 'Add New user',
        async handler(request) {

            const db = request.mongo.db;

            try {
                await db.collection('users').insert(request.payload);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'POST',
    path: '/throwAwayHistorys/add',
    config: {
        tags: ['api'],
        description: 'Add New user',
        async handler(request) {

            const db = request.mongo.db;

            try {
                await db.collection('throwAwayHistorys').insert(request.payload);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'POST',
    path: '/throwAwayHistorys/addPlas',
    config: {
        tags: ['api'],
        description: 'Add New throw away history',
        async handler(request) {

            const db = request.mongo.db;
            var dd = {
                user_id: ecobin01_usr,
                date_time: new Date(),
                plastic: 1,
                glass: 0,
                metal: 0
            }
            try {
                await db.collection('throwAwayHistorys').insert(dd);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});
server.route({
    method: 'POST',
    path: '/throwAwayHistorys/addGlass',
    config: {
        tags: ['api'],
        description: 'Add New throw away history',
        async handler(request) {

            const db = request.mongo.db;
            var dd = {
                user_id: ecobin01_usr,
                date_time: new Date(),
                plastic: 0,
                glass: 1,
                metal: 0
            }
            try {
                await db.collection('throwAwayHistorys').insert(dd);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});
server.route({
    method: 'POST',
    path: '/throwAwayHistorys/addMetal',
    config: {
        tags: ['api'],
        description: 'Add New throw away history',
        async handler(request) {

            const db = request.mongo.db;
            var dd = {
                user_id: ecobin01_usr,
                date_time: new Date(),
                plastic: 0,
                glass: 0,
                metal: 1
            }
            try {
                await db.collection('throwAwayHistorys').insert(dd);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'POST',
    path: '/rewards/add',
    config: {
        tags: ['api'],
        description: 'Add New reward',
        async handler(request) {

            const db = request.mongo.db;

            try {
                await db.collection('rewards').insert(request.payload);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'POST',
    path: '/promotions/add',
    config: {
        tags: ['api'],
        description: 'Add New promotion',
        async handler(request) {

            const db = request.mongo.db;

            try {
                await db.collection('promotions').insert(request.payload);
                return 'Insert Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'PUT',
    path: '/users/down_point/{id}',
    config: {
        tags: ['api'],
        description: 'Update point by user id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('users').update({ "_id": new ObjectID(request.params.id) }, { $inc: request.payload });
                return {
                    statuscode: 200,
                    message: 'ok'
                };
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});


//xxxxxxxxxxxxxxxxxxxxx
server.route({
    method: 'PUT',
    path: '/users/metal',
    config: {
        tags: ['api'],
        description: 'Update point by user id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('users').update({ "_id": new ObjectID(ecobin01_usr) }, { $inc: { point: 3 } });
                addMetalHistory();
                console.log("metal Response")
                console.log("...")
                return {
                    statuscode: 200,
                    message: 'ok'
                };
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});
//xxxxxxxxxxxxxxxxxxxxx
server.route({
    method: 'PUT',
    path: '/users/glass',
    config: {
        tags: ['api'],
        description: 'Update point by user id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('users').update({ "_id": new ObjectID(ecobin01_usr) }, { $inc: { point: 2 } });
                addGlassHistory();
                console.log("glass Response")
                console.log("...")
                return {
                    statuscode: 200,
                    message: 'ok'
                };
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});
//xxxxxxxxxxxxxxxxxxxxx
server.route({
    method: 'PUT',
    path: '/users/plastic',
    config: {
        tags: ['api'],
        description: 'Update point by user id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('users').update({ "_id": new ObjectID(ecobin01_usr) }, { $inc: { point: 1 } });
                addPlasHistory();
                console.log("plastic Response")
                console.log("...")
                return {
                    statuscode: 200,
                    message: 'ok'
                };
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'DELETE',
    path: '/users/delete/{id}',
    config: {
        tags: ['api'],
        description: 'Delete user by id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('users').remove({ _id: new ObjectID(request.params.id) });
                return 'Delete Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'DELETE',
    path: '/promotions/delete/{id}',
    config: {
        tags: ['api'],
        description: 'Delete promotion by id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('promotions').remove({ _id: new ObjectID(request.params.id) });
                return 'Delete Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'DELETE',
    path: '/rewards/delete/{id}',
    config: {
        tags: ['api'],
        description: 'Delete reward by id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('rewards').remove({ _id: new ObjectID(request.params.id) });
                return 'Delete Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'DELETE',
    path: '/rewards/rewardHistorys/delete/{id}',
    config: {
        tags: ['api'],
        description: 'Delete reward history by id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('rewardHistorys').remove({ _id: new ObjectID(request.params.id) });
                return 'Delete Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'DELETE',
    path: '/throwAwayHistorys/delete/{id}',
    config: {
        tags: ['api'],
        description: 'Delete throw away history by id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('throwAwayHistorys').remove({ _id: new ObjectID(request.params.id) });
                return 'Delete Complete';
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'PUT',
    path: '/bin/plastic',
    config: {
        tags: ['api'],
        description: 'Update point by user id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('bin').update({ '_id': ObjectID("5c44b29d442fa50b14068288") }, { $inc: { plasticQty: 1 } });

                return {
                    statuscode: 200,
                    message: 'ok'
                };
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'PUT',
    path: '/bin/glass',
    config: {
        tags: ['api'],
        description: 'Update point by user id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('bin').update({ '_id': ObjectID("5c44b29d442fa50b14068288") }, { $inc: { glassQty: 1 } });

                return {
                    statuscode: 200,
                    message: 'ok'
                };
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});


server.route({
    method: 'PUT',
    path: '/bin/metal',
    config: {
        tags: ['api'],
        description: 'Update point by user id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('bin').update({ '_id': ObjectID("5c44b29d442fa50b14068288") }, { $inc: { metalQty: 1 } });

                return {
                    statuscode: 200,
                    message: 'ok'
                };
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

server.route({
    method: 'PUT',
    path: '/bin/resetQty',
    config: {
        tags: ['api'],
        description: 'Update point by user id',
        async handler(request) {

            const db = request.mongo.db;
            const ObjectID = request.mongo.ObjectID;
            try {
                await db.collection('bin').update({ '_id': ObjectID("5c44b29d442fa50b14068288") }, { $set: { metalQty: 0, glassQty: 0, plasticQty: 0 } });

                return {
                    statuscode: 200,
                    message: 'ok'
                };
            } catch (err) {
                throw Boom.internal('Internal MongoDB error', err);
            }
        }
    }
});

//function 
function addPlasHistory() {

    request.post({ url: 'http://localhost:8000/throwAwayHistorys/addPlas', form: '' }, function(error, response, body) {
        console.log('Add Complete!')
        updatePlasQty();
    });
}

function addGlassHistory() {

    request.post({ url: 'http://localhost:8000/throwAwayHistorys/addGlass', form: '' }, function(error, response, body) {
        console.log('Add Complete!')
        updateGlassQty();
    });
}

function addMetalHistory() {

    request.post({ url: 'http://localhost:8000/throwAwayHistorys/addMetal', form: '' }, function(error, response, body) {
        console.log('Add Complete!')
        updateMetalQty();
    });
}
//Update
function updatePlasQty() {
    request.put('http://localhost:8000/bin/plastic', function(error, response, body) {
        broadcastBinData();
    });
}

function updateGlassQty() {
    request.put('http://localhost:8000/bin/glass', function(error, response, body) {
        broadcastBinData();
    });
}

function updateMetalQty() {
    request.put('http://localhost:8000/bin/metal', function(error, response, body) {
        broadcastBinData();
    });
}

function broadcastBinData() {
    request.get('http://localhost:8000/bin', function(error, response, body) {
        io.emit('binData', body);
    });
}

//sockets.io connect
io.sockets.on('connection', (socket) => {
    console.log('connected')

    socket.on('binData', function(msg) {

    });
});


// Start the server
const init = async() => {
    await server.register([{
            plugin: require('hapi-mongodb'),
            options: dbOpts
        },
        require('inert'),
        require('vision'),
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);
    await server.start();
    console.log(`Server started at ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {
    console.error(err);
    process.exit(1);
})

init();